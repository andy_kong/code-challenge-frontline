/**
 * 
 * ** Technical Assumptions **
 * 1. Application will be executed with the following requirements:
 * 1.a Node 6.x
 * 1.b NPM 3.x
 * 1.c Internet access to retrieve NPM dependency
 * 1.d Operating system has a Console, Bash, and/or Command Prompt
 * 
 * 2. Application will be executed from a command line using the following steps:
 * 2.a `npm install`
 * 2.b `node main`
 * 
 * 3. Application is not designed to be imported into another application as a module
 * 
 * 
 * ** Requirements Assumptions **
 * 1. Conversion should be done programmatically
 * 
 * 2. External libraries are allowed to simplify the application, provide optimal performance, and leverage production-tested logic
 * 
 * 3. The possible input characters are limited to the alpha letters (A-Z, a-z), the comma, and the open/close parentheses
 * 3.a All other characters can be removed from the input string and will not affect the output
 * 3.b The initial open parenthesis does not prepend a dash followed by a space to the output
 * 3.c All other open parentheses prepend dash(es) followed by a space to the output
 * 3.d All close parenthesis remove a dash from being prepended from the beginning of the next line if any exist
 * 3.e The comma, the open parenthesis, and the close parenthesis all append a new line to the output.
 * 
 * 
 */

// Define "modules"
var Introduction = function() {
    console.log('Welcome!');

    var header = 'Convert the string:'
        + '\n(id,created,employee(id,firstname,employeeType(id), lastname),location)'
        + '\nto the following output:\n'
        + '\nid'
        + '\ncreated'
        + '\nemployee'
        + '\n- id'
        + '\n- firstname'
        + '\n- employeeType'
        + '\n-- id'
        + '\n- lastname'
        + '\nlocation\n';

    console.log(header);
};

var ReadInput = function() { 
    var input = '(id,created,employee(id,firstname,employeeType(id), lastname),location)';
    console.log('Input:\n------\n' + input + '\n');
    
    return input;
};

var SanitizeInput = function(input) {
    var regexRemove = /[^A-Za-z\(\)\,]+/g;

    return input.replace(regexRemove, '');
};

var PrepInput = function(input) {
    // Remove first parenthesis if available
    return input.startsWith('(') ? input.substr(1) : input;
}

var ProcessInput = function(input, runningOutput, indent) {
    runningOutput = runningOutput || '';
    indent = indent || 0;

    var indentation = '';
    var nextChar;
    var excludeIndentation = false;

    if(!input) {
        return runningOutput;
    } else {
        nextChar = input[0];

        input = input.substr(1);
        switch(nextChar) {
            case '(':
                 runningOutput += '\n' + indentation;
                 indent++;
                 break;
            case ',':
                 runningOutput += '\n' + indentation;
                 indent;
                 break;
            case ')':
                 runningOutput;
                 indent--;
                 excludeIndentation = true;
                 break;
            default:
                 runningOutput += nextChar;
                 indent;
                 excludeIndentation = true;
                 break;
        }

        for(var i = 0; i < indent; i++) {
            indentation += '-';
        }
        indentation = indentation && !excludeIndentation ? indentation + ' ' : '';
        
        return ProcessInput(input, runningOutput + indentation, indent);
    }
};

var DisplayOutput = function(output) {
    console.log('Output:\n-------\n' + output + '\n');
};

var App = function() {
    Introduction();

    var input = ReadInput();
    input = SanitizeInput(input);
    input = PrepInput(input);

    var output = '';
    output = ProcessInput(input);
    DisplayOutput(output);
}

App();